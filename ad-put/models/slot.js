/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Slot', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    channel_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    media: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: 'all'
    },
    type: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    size_type_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    width: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    height: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    file_types: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    file_max: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    align_h: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    margin_h: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '0'
    },
    align_v: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    margin_v: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    stay_time: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    scrolled: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    closable: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    schedule_mode: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    ad_count: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    rotate_interval: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '5'
    },
    day_times: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.000'
    },
    enabled: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    create_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    update_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true
    } 
  }, {
      tableName: 'ads_slot'
  });
};