let Models = require('../models')

module.exports = {
    log: function (request, reply) {
        if(!request.query.putId || !request.query.clickUrl){
            return '';
        } 

        var clickUrl = decodeURIComponent(request.query.clickUrl); 
        
        Models.ClickLog.create({
            put_id: request.query.putId,
            click_url: clickUrl,
            from_ip: request.info.remoteAddress,
            from_url: decodeURIComponent(request.query.fromUrl),
            visitor: request.state.visitor.id
        });

        Models.sequelize.query(
            "UPDATE ads_put p \
                SET p.click_count = p.click_count + 1 \
              WHERE p.id = $id",
            {
                bind: {id: request.query.putId}
            });      

        return reply.redirect(clickUrl);
    }
};